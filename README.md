# Installation d'Atom


1.  Installer Atom
2.  Fermer toutes les onglets sauf l'onglet "Welcome Guide"
3.  Ouvrir ce [lien](https://atom.io/packages/gitlab-integration)
4.  Cliquer sur Install et choisir Atom dans la liste qui s'ouvre
5.  Depuis la page 'Welcome Guide' sur Atom ouvrir le menu 'Install Package' puis 'Open Installer'
6.  A gauche chosir l'onglet 'Packages' et cliquer sur 'Settings' sur le package gitlab-integration
7.  